#!/bin/sh
#
# Dieses Script bildet das `poky` Docker Image aus dem `Dockerfile` in
# diesem Verzeichnis. Das `poky` Image basiert auf dem  `buildhost` Container.
#
# # Parameter
#
# Dem Script kann optional der Name des Docker-Tags übergeben werden.
# Standardwert für diesen Tag ist `zzeroo/yocto/poky`
#
# ```
# sh ./build.sh test/yocto/poky
# ```
set -e

TAG=${1:-zzeroo/yocto/poky}

cd $(dirname $0)

docker build --no-cache --rm -t ${TAG} .

exit $?

Dieses Verzeichnis enthält ein Dockerfile mit dem ein Docker Image für ein
so genannten [buildhost] erzeugt werden kann.

# usage

Eine ausführlichere Beschreibung, wie diese Dateien zu verwenden sind, finden
Sie in der `README.md` im übergeordneten Verzeichnis.

## Docker Image bilden

Dem Skript kann optional der Name des Docker-Tags übergeben werden.
Standardwert für diesen Tag ist `zzeroo/yocto/buildhost`

```bash
sh ./build.sh
```

## Docker Container starten

Nachdem, mit dem Skript `build.sh`, das Docker Image erfolgreich erstellt wurde
kann der Docker Container gestartet werden.
Wurde `build.sh` ein alternatives Tag übergeben, dann muss dieses `run.sh`
mitgeteilt werden. 

```bash
sh ./run.sh
```


# Links

- https://www.yoctoproject.org/docs/latest/mega-manual/mega-manual.html#hardware-build-system-term
- https://www.yoctoproject.org/tools-resources/projects/toaster
- https://www.yoctoproject.org/docs/1.8/toaster-manual/toaster-manual.html

[build host]: https://www.yoctoproject.org/docs/latest/mega-manual/mega-manual.html#hardware-build-system-term

#!/bin/sh
#
# Dieses Script bildet das `buildhost` Docker Image aus dem `Dockerfile` in
# diesem Verzeichnis. Der buildhost Container ist die Grundlage aller weiteren
# Container/ Images dieses Projektes.
#
# # Parameter
#
# Dem Script kann optional der Name des Docker-Tags übergeben werden.
# Standardwert für diesen Tag ist `zzeroo/yocto/buildhost`
#
# ```
# sh ./build.sh test/yocto/buildhost
# ```
set -e

TAG=${1:-zzeroo/yocto/buildhost}

cd $(dirname $0)

docker build --no-cache --rm --tag ${TAG} .

exit $?

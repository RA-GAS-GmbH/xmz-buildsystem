#!/bin/bash
#
# Dieses Skript startet den `buildhost` Docker Container.
# Bei dem Start prüft das Skript zudem ob das Verzeichnis $SHAREDDIR vorhanden
# ist. Ist dieses nicht vorhanden wird es erstellt und die Dateisystemberechtigungen
# dieses Verzeichnisses werden angepasst.
set -e

cd $(dirname $0)

SHAREDDIR=${SHAREDDIR:-$PWD/shared}
CONTAINER=${CONTAINER:-buildhost}
IMAGE=${IMAGE:-zzeroo/yocto/buildhost}

# Create, if not present, a shared folder which will be used as working directory.
[[ -d $SHAREDDIR ]] || $(mkdir -p $SHAREDDIR && sudo chmod ug=rwX $SHAREDDIR && sudo chown 30000:300000 $SHAREDDIR)

# Try to start an existing/stopped container with the given name $CONTAINER.
# Otherwise, run a new one.
if docker inspect $CONTAINER >/dev/null 2>&1; then
    echo -e "\nINFO: Reattaching to running container '$CONTAINER'\n"
    docker start -i $CONTAINER
else
    echo -e "\nINFO: Creating a new container from image '$IMAGE'\n"
    docker run -ti \
      --volume=$SHAREDDIR:/home/build/shared \
      --name=$CONTAINER \
      $IMAGE
fi

exit $?

#!/bin/sh
#
#
#
# # Parameter
#
# Dem Script kann optional der Name des Docker-Tags übergeben werden.
# Standardwert für diesen Tag ist `zzeroo/yocto/opkg-updates`
#
# ```
# sh ./build.sh test/yocto/opkg-updates
# ```
set -e

TAG=${1:-zzeroo/yocto/opkg-updates}

cd $(dirname $0)

docker build --no-cache --rm -t ${TAG} .

exit $?

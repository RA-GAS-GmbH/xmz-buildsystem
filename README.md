# Buildsystem der xMZ-Plattform

Das Buildsystem setzt auf yocto/openembedded.

Zur besseren Trennung der Rechnerinfrastruktur werden die einzelnen
Komponenten in seperaten Docker Containern installliert.

## Quellcode auschecken

Dieses Repository ist Teil der [xMZ-Plattform][xmz].
Es wird empfohlen alle Komponenten der Plattform auszuchecken.

Der Quellcode kann mit folgendem Git Befehl herunter geladen werden.
Der Parameter `--recursive` sorgt dafür das alle Komponenten (git submodule)
ebenfalls herunter geladen werden.

```bash
git clone --recursive https://gitlab.com/RA-GAS-GmbH/xMZ-Plattform.git
```

Alternative kann, dieser Teil, auch einzeln heruntergeladen werden.

```bash
git clone https://gitlab.com/RA-GAS-GmbH/xmz-buildsystem.git
```


## Übersicht Komponenten und Verzeichnis Layout

- `buildhost`
  - die Dateien in diesem Verzeichnis erstellen und starte den so genannten
    buildhost. Dieses Docker Image bzw. dieser Docker Container bildet die
    Grundlage der weiteren Komponenten dieses Projekts.
- `poky`
  - dies ist der "Arbeits"-Conatiner in dem/ mit dem, der Quellcode gebaut wird.
    Siehe auch die README.md im `poky` Verzeichnis dieses Projekts.
- `opkg-shared`
  - dieser Container ist optional, er startet einen einfachen Webserver
    im 'ipk' Verzeichnis (ipk = installierbare Packete). Das `ipk` Verzeichnis
    ist ein Unterverzeichnis im Shared Ordner.
    Siehe auch die README.md im `opkg-shared` Verzeichnis dieses Projekts.
- `shared`
  - gemainsames Verzeichnis des `xmz-buildsystems`
  - in diesem Ordner sind die gemeinsamen Dateien wie:
        Downloads, bitbake Cache, Artefacts und so weiter, zu finden
  - **In diesem Verzeichnis müssen auch evtl. benötigte Layer mit weiteren `.bb`
    Rezepten gespeichert werden.**

## Abhänigkeiten

Das Buildsystem benötigtDocker. Die Docker Installation ist u.a. auf dieser
Webseite beschrieben https://docs.docker.com/install/linux/docker-ce/debian/#set-up-the-repository

## Installation der Docker Container

Die Reihenfolge in der die Docker Container erstellt werden ist wichtig!

Begonnen wird mit dem Basis Container `buildhost`.

### Yocto `buildhost`

Die Basis bildet der so genannte `buildhost` Container. **Dieses Docker Image muss
als erstes erstellt werden!**

```bash
sh ./buildhost/build.sh
```

### Poky

Nach dem der Container `buildhost` erstellt wurde wird der Conainter `poky`
erstellt.

```bash
sh ./poky/build.sh
```

### Optional: opkg-updates Server

Nachdem der Basis Conainter 'yocto' erstellt wurde kann auch der 'opkg-updates'
Container erstellt werden.

```bash
sh ./opkg-updates/build.sh
```

# Shared

Im 'poky'-Container ist das Buildtool `poky` (/home/build/poky) enthalten.
Alle weiteren Rezepte, sowie die `bitbake` Konfigurationsdateien
sind in Verzeichnis `shared` zu finden. Bzw. müssen in dieses Verzeichnis
kopiert werde.

## Zugriffsrechte `shared`

In den Docker Containern ist ein Benutzer `build` (UserID:30000) mit der
Gruppe `build` (GroupID:30000) angelegt.

**Dieser Benutzer/Gruppe sollte auch auf dem Buildsystem existieren.**
Die folgenden Befehle legen den Benutzer `build` und die Grupper `build`
mit der GruppenID 30000 an.

```bash
groupadd -g 30000 build
useradd -g build -u 30000 build
```

Nun muss noch der aktuelle Benutzer in die Gruppe `build` eingebunden werden.

```bash
gpasswd -a $USER build
```

Gleichzeitig ist in den Docker Containeren die `umask` gesetzt,
auch diese sollte auf dem Buildsystem gesetzt sein.

Unter Debian/ Ubuntu geschieht das in der Datei `/etc/login.defs`.

Die `login.defs` muss eventuel aktiviert werden, dazu muss in der Datei
`/etc/pam.d/common-session` die folgende Zeile vorhanden sein:

```bash
# /etc/pam.d/common-session
session optional pam_umask.so
```

In der Datei `/etc/login.defs` wird dann die UMASK gesetzt.

```bash
# /etc/login.defs
UMASK           002
```

Siehe dazu auch: <https://stackoverflow.com/questions/10220531/how-to-set-system-wide-umask>


## Recipes

Die 'meta' Repository mit essentiellen Layern und Rezepten sind als
`git submodule` in diesem Repository eingebunden.
Diese Submodule können mit den folgenden Befehlen syncronisiert werden.

```bash
git submodule init
git submodule update
```


# Software build

Es gibt verschiedene Arten wie mit dem Buildsystem gearbeitet werden kann.

## poky "interactive"

Eine Möglichkeit ist die interactive Arbeit mit `poky`. Dazu muss Docker mit
den folgenden Parametern gestartet werden.

```bash
docker run --volume=/abs/path/to/shared:/home/build/shared --rm -ti zzeroo/poky:latest
```

Dieser Befehl startet eine Bash Shell in der die open-embedded Umgebung bereits
"gesourced" wurde (siehe [`poky` Dockerfile](poky/Dockerfile#L12)).

## poky "scripted"

`poky` kann im Docker Container auch direkt aufgerufen werden. Das ist sehr
hilfreich wenn man das Buildsystem zum Beispiel in Scripten aufrufen will.

Im folgenden Beispiel wird das Rezept `xmz-test-tool-image` mit dem Buildsystem
gebaut.

```bash
docker run --volume=/abs/path/to/shared:/home/build/shared --rm -ti zzeroo/poky:latest  "cd /home/build/poky && source oe-init-build-env /home/build/shared/build && poky xmz-test-tool-image"
```



# Publish Artefacts
## last build log

```bash
/var/src/xMZ-Plattform/xmz-buildsystem/shared/build/tmp/log/cooker/bananapro
```

## Stats
### most current build date
```bash
ls -1t /var/src/xMZ-Plattform/xmz-buildsystem/shared/build/tmp/buildstats/| tail -n1
```



[xmz]: https://gitlab.com/RA-GAS-GmbH/xMZ-Plattform
